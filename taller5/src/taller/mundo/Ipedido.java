package taller.mundo;

public interface Ipedido extends Comparable<Pedido> {
	
	public double getPrecio();
	public boolean getAtendido();
	public void setAtendido();
	public String getAutorPedido();
	public int getCercania() ;
	
}
