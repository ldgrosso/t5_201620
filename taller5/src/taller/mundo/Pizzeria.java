package taller.mundo;


public class Pizzeria 
{	
	// ----------------------------------
    // Constantes
    // ----------------------------------
	
	/**
	 * Constante que define la accion de recibir un pedido
	 */
	public final static String RECIBIR_PEDIDO = "RECIBIR";
	/**
	 * Constante que define la accion de atender un pedido
	 */
	public final static String ATENDER_PEDIDO = "ATENDER";
	/**
	 * Constante que define la accion de despachar un pedido
	 */
	public final static String DESPACHAR_PEDIDO = "DESPACHAR";
	/**
	 * Constante que define la accion de finalizar la secuencia de acciones
	 */
	public final static String FIN = "FIN";
	
	// ----------------------------------
    // Atributos
    // ----------------------------------
	public Pedido[] listaAtender;
	public int atendidos = 0;
	/**
	 * Heap que almacena los pedidos recibidos
	 */
	private Heap<Pedido> recibidos;
	/**
	 * Getter de pedidos recibidos
	 */
	public Heap<Pedido> getRecibidos(){
		return recibidos;
	}
 	/** 
	 * Heap de elementos por despachar
	 */
	private Heap<Pedido> despachar;
	/**
	 * Getter de elementos por despachar
	 */
	public Heap<Pedido> getDespachar(){
		return despachar;
	}
	
	
	// ----------------------------------
    // Constructor
    // ----------------------------------
 
	/**
	 * Constructor de la case Pizzeria
	 */
	public Pizzeria()
	{
		recibidos = new Heap<Pedido>(true);
		despachar = new Heap<Pedido>(false);
		
	}
	
	// ----------------------------------
    // Métodos
    // ----------------------------------
	
	/**
	 * Agrega un pedido a la cola de prioridad de pedidos recibidos
	 * @param nombreAutor nombre del autor del pedido
	 * @param precio precio del pedido 
	 * @param cercania cercania del autor del pedido 
	 */
	public void agregarPedido(String nombreAutor, double precio, int cercania)
	{
		Pedido nuevo = new Pedido(precio, nombreAutor, cercania);
		recibidos.add(nuevo);
		
	}
	
	// Atender al pedido más importante de la cola
	
	/**
	 * Retorna el proximo pedido en la cola de prioridad o null si no existe.
	 * @return p El pedido proximo en la cola de prioridad
	 */
	public Pedido atenderPedido()
	{
		atendidos++;
		if(despachar.getheap(1)==null){
			atendidos = 1;
			int i = 0;
			while(i<listaAtender.length&&listaAtender[i] !=null){
				listaAtender[i].setAtendido();
				despachar.add(listaAtender[i]);
				i++;
			}
		}
		
		return listaAtender[atendidos-1] ;
	}

	/**
	 * Despacha al pedido proximo a ser despachado. 
	 * @return Pedido proximo pedido a despachar
	 */
	public Pedido despacharPedido()
	{
		
	    return  despachar.poll();
	}
	
	 /**
     * Retorna la cola de prioridad de pedidos recibidos como un arreglo. 
     * @return arreglo de pedidos recibidos manteniendo el orden de la cola de prioridad.
     */
     public Pedido [] pedidosRecibidosList()
     {
    	 Pedido[] resp = new Pedido[recibidos.size()];
    	 Pedido[] nuevo = new Pedido[recibidos.size()-atendidos];
       
    	 
    	if(atendidos==0){
    		for (int i = 0; i < recibidos.size(); i++) {
			resp[i]=(Pedido) recibidos.getheap(i+1);
    		}
    		listaAtender = resp;
    	}
    	else{
    		for (int i = 0; i < nuevo.length; i++) {
				nuevo[i]=listaAtender[atendidos+i];
			}
    		resp = nuevo;
    	}
        return  resp;
     }
     
      /**
       * Retorna la cola de prioridad de despachos como un arreglo. 
       * @return arreglo de despachos manteniendo el orden de la cola de prioridad.
       */
     public Pedido [] colaDespachosList()
     {
    	 Pedido[] resp = new Pedido[despachar.size()];
         for (int i = 0; i < despachar.size(); i++) {
 			resp[i]= (Pedido) despachar.getheap(i+1);
 		} 
         return  resp;
     }
}
