package taller.mundo;

import taller.estructuras.IHeap;

public class Heap <T extends Comparable<T>> implements IHeap<T>{

	private T[] heap;
	private int tamanio;
	private boolean max;
	
	
	public  Heap (boolean minmax){
		heap = (T[]) new Comparable[20] ;
		tamanio = 0;
		max = minmax;
	}
	public void add(T elemento) {
		
		heap[++tamanio] = elemento;
		
		siftUp();
	}

	public T peek() {
		T primero = heap[1];
		return primero;
	}

	@Override
	public T poll() {
		T primero = heap[1];
		exch(1, tamanio--);
		heap[tamanio+1]=null;
		siftDown();
		return primero;
	}

	@Override
	public int size() {
		return tamanio;
	}

	
	public boolean isEmpty() {
		return tamanio == 0;
	}

	@Override
	/**
	 * Mueve el último elemento arriba en el arbol, mientras que sea necesario.
	 * Es usado para restaurar la condición de heap luego de inserción.
	 */
	
	public void siftUp() {
		int k = tamanio;
		if(this.max = true){
			
			while(k>1&& less(k/2,k)){
				exch(k/2, k);
				k = k/2;
			}
			if(less(k/2+1,k)){
				exch(k/2+1,k);
			}
			if((k-1)%2==0&& k>2&& less(k-1, k)){
				exch(k-1, k);
			}
		}
		else {
			while(k>1&& less(k,k/2)){
				exch(k, k/2);
				k = k/2;
			}
			/**
			if((k-1)%2==0&& k>2&& less(k, k-1)){
				exch(k, k-1);
			}
			**/
			
		}
		
	}

	@Override
	public void siftDown() {
		int k = 1;
		if(max == true){
			while(2*k<=tamanio){
				int j = 2*k;
				if(j<tamanio&& less(j,j+1)) j++;
				if(!less(k,j)) break;
				exch(k,j);
				k=j;
			}
		}
		else{
			while(2*k<=tamanio){
				int j = 2*k;
				if(j<tamanio&& less(j+1,j)) j++;
				if(!less(j,k)) break;
				exch(j,k);
				k=j;
			}
		}
		
		
	}

	public T getheap(int i ){
		T tem = heap[i];
		return tem;
	}
	private boolean less(int i , int j ){
		
		return heap[i].compareTo(heap[j])<0;
	}
	private void exch(int i, int j ){
		T temp = heap[i]; heap[i] = heap[j]; heap[j]=temp;
	}
	public void sort(){
		int N = tamanio;
		while(N>1){
			exch(1,N--);
			sink(1,N);
		}
	}
	private void sink ( int k, int N){
		if(max == true){
			while(2*k<=N){
				int j = 2*k;
				if(j<N&& less(j,j+1)) j++;
				if(!less(k,j)) break;
				exch(k,j);
				k=j;
			}
		}
		else{
			while(2*k<=N){
				int j = 2*k;
				if(j<N&& less(j+1,j)) j++;
				if(!less(j,k)) break;
				exch(j,k);
				k=j;
			}
		}
		
		
		
	}
	
}
