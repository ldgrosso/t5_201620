package taller.mundo;

public  class Pedido implements Ipedido
{

	// ----------------------------------
	// Atributos
	// ----------------------------------

	/**
	 * Precio del pedido
	 */
	private double precio;
	
	/**
	 * Autor del pedido
	 */
	private String autorPedido;
	
	/**
	 * Cercania del pedido
	 */
	private int cercania;
	
	private boolean atendido;
	// ----------------------------------
	// Constructor
	// ----------------------------------
	
	/**
	 * Constructor del pedido
	 */
	public Pedido (double pPrecio, String pNombre, int pCercania){
		precio = pPrecio;
		autorPedido=pNombre;
		cercania= pCercania;
		atendido = false;
	}
	
	// ----------------------------------
	// Métodos
	// ----------------------------------
	
	/**
	 * Getter del precio del pedido
	 */
	public double getPrecio()
	{
		return precio;
	}
	public boolean getAtendido(){
		return atendido;
	}
	public void setAtendido(){
		atendido = true;
	}
	
	/**
	 * Getter del autor del pedido
	 */
	public String getAutorPedido()
	{
		return autorPedido;
	}
	
	/**
	 * Getter de la cercania del pedido
	 */
	public int getCercania() {
		return cercania;
	}




	@Override
	public int compareTo(Pedido o) {
		int i = 0;
		if(!atendido){
			if ((this.getPrecio()>o.getPrecio())) {
				i = 1;
			}
			else if(this.getPrecio()<o.getPrecio()){ 
				i = -1;
			}
		}
		else{
			if ((this.getCercania()>o.getCercania())) {
				i = -1;
			}
			else if(this.getCercania()<o.getCercania()){ 
				i = 1;
			}
		}
			
		return i ;
	}

	

	
}
